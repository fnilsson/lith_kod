#include <iostream>
#include <list>
#include <iterator>
#include <algorithm>

using namespace std;

class Stack{
private:
  list<int> _body;
public:
  Stack(list<int> &body):_body(body){}
  ~Stack(){}
 
  int pop()
  {
    int temp = _body.front();
    _body.pop_front();
    return temp;
  }
  void push(int x)
  {
    _body.push_front(x);
  }
  int top()
  {
    return _body.front();
  }
  int size()
  {
    return _body.size();
  } 
  bool empty()
  {
    return _body.empty();
  }
  void print()
  {
    for(auto it = _body.begin(); it != _body.end(); ++it)
      cout << *it << " ";
    cout << endl;
  }
};

int main()
{
  list<int> body = {1, 2, 3, 4};
  Stack stack(body);
  stack.print();
  stack.pop();
  cout << "F�rsta elementet i stacken: " << stack.top() << endl;
  stack.push(5);
  cout << "F�rsta elementet i stacken: " << stack.top() << endl;
  stack.print();
  cout << "�r stacken tom: " << boolalpha << stack.empty() << endl;
  cout << "Antal element i stacken: " << stack.size() << endl;
  return 0;
}
