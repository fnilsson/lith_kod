#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>

using namespace std;

class Stack{
private:
  vector<int> _body;
public:
  Stack(vector<int> &body):_body(body){}
  ~Stack(){}
 
  int pop()
  {
    return *(_body.erase(_body.begin()));
  }
  void push(int x)
  {
    _body.insert(_body.begin(), x);
  }
  int top()
  {
    return _body.front();
  }
  int size()
  {
    return _body.size();
  } 
  bool empty()
  {
    return _body.empty();
  }
  void print()
  {
    for(auto it = _body.begin(); it != _body.end(); ++it)
      cout << *it << " ";
    cout << endl;
  }
};

int main()
{
  vector<int> body = {1, 2, 3, 4};
  Stack stack(body);
  stack.print();
  stack.pop();
  cout << "F�rsta elementet i stacken: " << stack.top() << endl;
  stack.push(5);
  cout << "F�rsta elementet i stacken: " << stack.top() << endl;
  stack.print();
  cout << "�r stacken tom: " << boolalpha << stack.empty() << endl;
  cout << "Antal element i stacken: " << stack.size() << endl;
  return 0;
}
