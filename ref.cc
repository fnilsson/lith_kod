#include <iostream>

using namespace std;

void add_five(int x)
{
    x = x + 5;
    cout << "Value of x in the function: " << x << endl;
}
void add_three(int& x)
{
    x = x + 3;
    cout << "Value of x in the function: " << x << endl;
}

int main()
{
    int x = 0;
    add_five(x);
    cout << "Value of x: " << x << endl;
    add_three(x);
    cout << "Value of x: " << x << endl;
    return 0;
}
