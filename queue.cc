#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>

using namespace std;

class Queue{
private:
  vector<int> _body;
public:
  Queue(vector<int> &body):_body(body){}
  ~Queue(){}
 
  void pop()
  {
    _body.pop_back();
  }
  void push(int x)
  {
    _body.insert(_body.begin(), x);
  }
  int front()
  {
    return _body.front();
  }
  int back()
  {
    return _body.back();
  }
  int size()
  {
    return _body.size();
  }
  bool empty()
  {
    return _body.empty();
  }
  void print()
  {
    for(auto it = _body.begin(); it != _body.end(); ++it)
      cout << *it << " ";
    cout << endl;
  }
};

int main()
{
  vector<int> body = {1, 2, 3, 4};
  Queue queue(body);
  queue.print();
  queue.pop();
  cout << "Sista elementet i k�n: " << queue.back() << endl;
  queue.push(5);
  cout << "F�rsta elementet i k�n: " << queue.front() << endl;
  queue.print();
  cout << "�r k�n tom: " << boolalpha << queue.empty() << endl;
  cout << "Antal element i k�n: " << queue.size() << endl;
  return 0;
}
